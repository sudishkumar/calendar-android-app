package com.edugaon.calendarapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CalendarView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val calendarView = findViewById<CalendarView>(R.id.calendarView)
        calendarView.setMinDate(System.currentTimeMillis() - 1000);

        // advance things
        calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            val monthValue =  month + 1
            Toast.makeText(this, "$dayOfMonth-$monthValue-$year", Toast.LENGTH_LONG).show()
        }
    }
}